# Silabs device specific code

This repo contain startup, system execution and mcu hw specific code from [Silabs Gecko SDK][silabsdoc_link]
Contact [maintainer][maintainer_email] for information and/or questions


## Disclaimer

We are not the original author of this code, Silabs is the [owner/maintainer][silabsdoc_link] of the original source code.  
This repo exist only to collect and make multi-arch compatible file collated from files in the _sdk folder_ of [SS][ss_link].  
This is only needed because there is no official git repo maintained containing this code and no official version giving access to multiple family at the same time.


## Install

1. Include the `inc` folder in your search path
2. Add the `src` folder to your compiler path
3. Exclude the unwanted device's file from the compile path (header can be ignored they are already protected)
    - ie, for GG11B, exclude all file in _src_ **except startup_efm32gg1b.S** and **system_efm32gg11b.c**

[silabsdoc_link]:   https://docs.silabs.com/
[ss_link]:          https://www.silabs.com/products/development-tools/software/simplicity-studio
[maintainer_email]: <MAILTO:laurencedv@realee.tech>

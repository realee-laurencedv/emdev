/***************************************************************************//**
 * @file
 * @brief CMSIS Cortex-M Peripheral Access Layer for Silicon Laboratories
 *        microcontroller devices
 *
 * This is a convenience header file for defining the part number on the
 * build command line, instead of specifying the part specific header file.
 *
 * @verbatim
 * Example: Add "-DEFM32G890F128" to your build options, to define part
 *          Add "#include "em_device.h" to your source files
 * @endverbatim
 *
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * SPDX-License-Identifier: Zlib
 *
 * The licensor of this software is Silicon Laboratories Inc.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 ******************************************************************************/

#ifndef EM_DEVICE_H
#define EM_DEVICE_H


/* Testable device header */
#if defined(TLIBTEST)
	/* ======== TG11B ======== */

	/* ======== JG12B ======== */
	/* ======== PG12B ======== */

	/* ======== GG11B ======== */
	#if defined(EFM32GG11B820F2048GL192)
	#include "testable_efm32gg11b820f2048gl192.h"
	
	/* ======== GG12B ======== */
	
	#else
	#error "em_device.h: testing with unsupported PART NUMBER (or PART NUMBER undefined)"
	
	#endif


/* Real device header */
#else
	/* ======== TG11B ======== */
	#if defined(EFM32TG11B120F128GM32)
	#include "efm32tg11b120f128gm32.h"

	#elif defined(EFM32TG11B120F128GM64)
	#include "efm32tg11b120f128gm64.h"

	#elif defined(EFM32TG11B120F128GQ48)
	#include "efm32tg11b120f128gq48.h"

	#elif defined(EFM32TG11B120F128GQ64)
	#include "efm32tg11b120f128gq64.h"

	#elif defined(EFM32TG11B120F128IM32)
	#include "efm32tg11b120f128im32.h"

	#elif defined(EFM32TG11B120F128IM64)
	#include "efm32tg11b120f128im64.h"

	#elif defined(EFM32TG11B120F128IQ48)
	#include "efm32tg11b120f128iq48.h"

	#elif defined(EFM32TG11B120F128IQ64)
	#include "efm32tg11b120f128iq64.h"

	#elif defined(EFM32TG11B140F64GM32)
	#include "efm32tg11b140f64gm32.h"

	#elif defined(EFM32TG11B140F64GM64)
	#include "efm32tg11b140f64gm64.h"

	#elif defined(EFM32TG11B140F64GQ48)
	#include "efm32tg11b140f64gq48.h"

	#elif defined(EFM32TG11B140F64GQ64)
	#include "efm32tg11b140f64gq64.h"

	#elif defined(EFM32TG11B140F64IM32)
	#include "efm32tg11b140f64im32.h"

	#elif defined(EFM32TG11B140F64IM64)
	#include "efm32tg11b140f64im64.h"

	#elif defined(EFM32TG11B140F64IQ48)
	#include "efm32tg11b140f64iq48.h"

	#elif defined(EFM32TG11B140F64IQ64)
	#include "efm32tg11b140f64iq64.h"

	#elif defined(EFM32TG11B320F128GM64)
	#include "efm32tg11b320f128gm64.h"

	#elif defined(EFM32TG11B320F128GQ48)
	#include "efm32tg11b320f128gq48.h"

	#elif defined(EFM32TG11B320F128GQ64)
	#include "efm32tg11b320f128gq64.h"

	#elif defined(EFM32TG11B320F128IM64)
	#include "efm32tg11b320f128im64.h"

	#elif defined(EFM32TG11B320F128IQ48)
	#include "efm32tg11b320f128iq48.h"

	#elif defined(EFM32TG11B320F128IQ64)
	#include "efm32tg11b320f128iq64.h"

	#elif defined(EFM32TG11B340F64GM64)
	#include "efm32tg11b340f64gm64.h"

	#elif defined(EFM32TG11B340F64GQ48)
	#include "efm32tg11b340f64gq48.h"

	#elif defined(EFM32TG11B340F64GQ64)
	#include "efm32tg11b340f64gq64.h"

	#elif defined(EFM32TG11B340F64IM64)
	#include "efm32tg11b340f64im64.h"

	#elif defined(EFM32TG11B340F64IQ48)
	#include "efm32tg11b340f64iq48.h"

	#elif defined(EFM32TG11B340F64IQ64)
	#include "efm32tg11b340f64iq64.h"

	#elif defined(EFM32TG11B520F128GM32)
	#include "efm32tg11b520f128gm32.h"

	#elif defined(EFM32TG11B520F128GM64)
	#include "efm32tg11b520f128gm64.h"

	#elif defined(EFM32TG11B520F128GM80)
	#include "efm32tg11b520f128gm80.h"

	#elif defined(EFM32TG11B520F128GQ48)
	#include "efm32tg11b520f128gq48.h"

	#elif defined(EFM32TG11B520F128GQ64)
	#include "efm32tg11b520f128gq64.h"

	#elif defined(EFM32TG11B520F128GQ80)
	#include "efm32tg11b520f128gq80.h"

	#elif defined(EFM32TG11B520F128IM32)
	#include "efm32tg11b520f128im32.h"

	#elif defined(EFM32TG11B520F128IM64)
	#include "efm32tg11b520f128im64.h"

	#elif defined(EFM32TG11B520F128IM80)
	#include "efm32tg11b520f128im80.h"

	#elif defined(EFM32TG11B520F128IQ48)
	#include "efm32tg11b520f128iq48.h"

	#elif defined(EFM32TG11B520F128IQ64)
	#include "efm32tg11b520f128iq64.h"

	#elif defined(EFM32TG11B520F128IQ80)
	#include "efm32tg11b520f128iq80.h"

	#elif defined(EFM32TG11B540F64GM32)
	#include "efm32tg11b540f64gm32.h"

	#elif defined(EFM32TG11B540F64GM64)
	#include "efm32tg11b540f64gm64.h"

	#elif defined(EFM32TG11B540F64GM80)
	#include "efm32tg11b540f64gm80.h"

	#elif defined(EFM32TG11B540F64GQ48)
	#include "efm32tg11b540f64gq48.h"

	#elif defined(EFM32TG11B540F64GQ64)
	#include "efm32tg11b540f64gq64.h"

	#elif defined(EFM32TG11B540F64GQ80)
	#include "efm32tg11b540f64gq80.h"

	#elif defined(EFM32TG11B540F64IM32)
	#include "efm32tg11b540f64im32.h"

	#elif defined(EFM32TG11B540F64IM64)
	#include "efm32tg11b540f64im64.h"

	#elif defined(EFM32TG11B540F64IM80)
	#include "efm32tg11b540f64im80.h"

	#elif defined(EFM32TG11B540F64IQ48)
	#include "efm32tg11b540f64iq48.h"

	#elif defined(EFM32TG11B540F64IQ64)
	#include "efm32tg11b540f64iq64.h"

	#elif defined(EFM32TG11B540F64IQ80)
	#include "efm32tg11b540f64iq80.h"


	/* ======== JG12B ======== */
	#elif defined(EFM32JG12B500F1024GL125)
	#include "efm32jg12b500f1024gl125.h"

	#elif defined(EFM32JG12B500F1024GM48)
	#include "efm32jg12b500f1024gm48.h"

	#elif defined(EFM32JG12B500F1024IL125)
	#include "efm32jg12b500f1024il125.h"

	#elif defined(EFM32JG12B500F1024IM48)
	#include "efm32jg12b500f1024im48.h"


	/* ======== PG12B ======== */
	#elif defined(EFM32PG12B500F1024GL125)
	#include "efm32pg12b500f1024gl125.h"

	#elif defined(EFM32PG12B500F1024GM48)
	#include "efm32pg12b500f1024gm48.h"

	#elif defined(EFM32PG12B500F1024IL125)
	#include "efm32pg12b500f1024il125.h"

	#elif defined(EFM32PG12B500F1024IM48)
	#include "efm32pg12b500f1024im48.h"


	/* ======== GG11B ======== */
	#elif defined(EFM32GG11B110F2048GM64)
	#include "efm32gg11b110f2048gm64.h"

	#elif defined(EFM32GG11B110F2048GQ64)
	#include "efm32gg11b110f2048gq64.h"

	#elif defined(EFM32GG11B110F2048IM64)
	#include "efm32gg11b110f2048im64.h"

	#elif defined(EFM32GG11B110F2048IQ64)
	#include "efm32gg11b110f2048iq64.h"

	#elif defined(EFM32GG11B120F2048GM64)
	#include "efm32gg11b120f2048gm64.h"

	#elif defined(EFM32GG11B120F2048GQ64)
	#include "efm32gg11b120f2048gq64.h"

	#elif defined(EFM32GG11B120F2048IM64)
	#include "efm32gg11b120f2048im64.h"

	#elif defined(EFM32GG11B120F2048IQ64)
	#include "efm32gg11b120f2048iq64.h"

	#elif defined(EFM32GG11B310F2048GL112)
	#include "efm32gg11b310f2048gl112.h"

	#elif defined(EFM32GG11B310F2048GQ100)
	#include "efm32gg11b310f2048gq100.h"

	#elif defined(EFM32GG11B320F2048GL112)
	#include "efm32gg11b320f2048gl112.h"

	#elif defined(EFM32GG11B320F2048GQ100)
	#include "efm32gg11b320f2048gq100.h"

	#elif defined(EFM32GG11B420F2048GL112)
	#include "efm32gg11b420f2048gl112.h"

	#elif defined(EFM32GG11B420F2048GL120)
	#include "efm32gg11b420f2048gl120.h"

	#elif defined(EFM32GG11B420F2048GM64)
	#include "efm32gg11b420f2048gm64.h"

	#elif defined(EFM32GG11B420F2048GQ100)
	#include "efm32gg11b420f2048gq100.h"

	#elif defined(EFM32GG11B420F2048GQ64)
	#include "efm32gg11b420f2048gq64.h"

	#elif defined(EFM32GG11B420F2048IL112)
	#include "efm32gg11b420f2048il112.h"

	#elif defined(EFM32GG11B420F2048IL120)
	#include "efm32gg11b420f2048il120.h"

	#elif defined(EFM32GG11B420F2048IM64)
	#include "efm32gg11b420f2048im64.h"

	#elif defined(EFM32GG11B420F2048IQ100)
	#include "efm32gg11b420f2048iq100.h"

	#elif defined(EFM32GG11B420F2048IQ64)
	#include "efm32gg11b420f2048iq64.h"

	#elif defined(EFM32GG11B510F2048GL120)
	#include "efm32gg11b510f2048gl120.h"

	#elif defined(EFM32GG11B510F2048GM64)
	#include "efm32gg11b510f2048gm64.h"

	#elif defined(EFM32GG11B510F2048GQ100)
	#include "efm32gg11b510f2048gq100.h"

	#elif defined(EFM32GG11B510F2048GQ64)
	#include "efm32gg11b510f2048gq64.h"

	#elif defined(EFM32GG11B510F2048IL120)
	#include "efm32gg11b510f2048il120.h"

	#elif defined(EFM32GG11B510F2048IM64)
	#include "efm32gg11b510f2048im64.h"

	#elif defined(EFM32GG11B510F2048IQ100)
	#include "efm32gg11b510f2048iq100.h"

	#elif defined(EFM32GG11B510F2048IQ64)
	#include "efm32gg11b510f2048iq64.h"

	#elif defined(EFM32GG11B520F2048GL120)
	#include "efm32gg11b520f2048gl120.h"

	#elif defined(EFM32GG11B520F2048GM64)
	#include "efm32gg11b520f2048gm64.h"

	#elif defined(EFM32GG11B520F2048GQ100)
	#include "efm32gg11b520f2048gq100.h"

	#elif defined(EFM32GG11B520F2048GQ64)
	#include "efm32gg11b520f2048gq64.h"

	#elif defined(EFM32GG11B520F2048IL120)
	#include "efm32gg11b520f2048il120.h"

	#elif defined(EFM32GG11B520F2048IM64)
	#include "efm32gg11b520f2048im64.h"

	#elif defined(EFM32GG11B520F2048IQ100)
	#include "efm32gg11b520f2048iq100.h"

	#elif defined(EFM32GG11B520F2048IQ64)
	#include "efm32gg11b520f2048iq64.h"

	#elif defined(EFM32GG11B820F2048GL120)
	#include "efm32gg11b820f2048gl120.h"

	#elif defined(EFM32GG11B820F2048GL152)
	#include "efm32gg11b820f2048gl152.h"

	#elif defined(EFM32GG11B820F2048GL192)
	#include "efm32gg11b820f2048gl192.h"

	#elif defined(EFM32GG11B820F2048GM64)
	#include "efm32gg11b820f2048gm64.h"

	#elif defined(EFM32GG11B820F2048GQ100)
	#include "efm32gg11b820f2048gq100.h"

	#elif defined(EFM32GG11B820F2048GQ64)
	#include "efm32gg11b820f2048gq64.h"

	#elif defined(EFM32GG11B820F2048IL120)
	#include "efm32gg11b820f2048il120.h"

	#elif defined(EFM32GG11B820F2048IL152)
	#include "efm32gg11b820f2048il152.h"

	#elif defined(EFM32GG11B820F2048IM64)
	#include "efm32gg11b820f2048im64.h"

	#elif defined(EFM32GG11B820F2048IQ100)
	#include "efm32gg11b820f2048iq100.h"

	#elif defined(EFM32GG11B820F2048IQ64)
	#include "efm32gg11b820f2048iq64.h"

	#elif defined(EFM32GG11B840F1024GL120)
	#include "efm32gg11b840f1024gl120.h"

	#elif defined(EFM32GG11B840F1024GL152)
	#include "efm32gg11b840f1024gl152.h"

	#elif defined(EFM32GG11B840F1024GL192)
	#include "efm32gg11b840f1024gl192.h"

	#elif defined(EFM32GG11B840F1024GM64)
	#include "efm32gg11b840f1024gm64.h"

	#elif defined(EFM32GG11B840F1024GQ100)
	#include "efm32gg11b840f1024gq100.h"

	#elif defined(EFM32GG11B840F1024GQ64)
	#include "efm32gg11b840f1024gq64.h"

	#elif defined(EFM32GG11B840F1024IL120)
	#include "efm32gg11b840f1024il120.h"

	#elif defined(EFM32GG11B840F1024IL152)
	#include "efm32gg11b840f1024il152.h"

	#elif defined(EFM32GG11B840F1024IM64)
	#include "efm32gg11b840f1024im64.h"

	#elif defined(EFM32GG11B840F1024IQ100)
	#include "efm32gg11b840f1024iq100.h"

	#elif defined(EFM32GG11B840F1024IQ64)
	#include "efm32gg11b840f1024iq64.h"


	/* ======== GG12B ======== */
	#elif defined(EFM32GG12B110F1024GM64)
	#include "efm32gg12b110f1024gm64.h"

	#elif defined(EFM32GG12B110F1024GQ64)
	#include "efm32gg12b110f1024gq64.h"

	#elif defined(EFM32GG12B110F1024IM64)
	#include "efm32gg12b110f1024im64.h"

	#elif defined(EFM32GG12B110F1024IQ64)
	#include "efm32gg12b110f1024iq64.h"

	#elif defined(EFM32GG12B130F512GM64)
	#include "efm32gg12b130f512gm64.h"

	#elif defined(EFM32GG12B130F512GQ64)
	#include "efm32gg12b130f512gq64.h"

	#elif defined(EFM32GG12B130F512IM64)
	#include "efm32gg12b130f512im64.h"

	#elif defined(EFM32GG12B130F512IQ64)
	#include "efm32gg12b130f512iq64.h"

	#elif defined(EFM32GG12B310F1024GL112)
	#include "efm32gg12b310f1024gl112.h"

	#elif defined(EFM32GG12B310F1024GQ100)
	#include "efm32gg12b310f1024gq100.h"

	#elif defined(EFM32GG12B330F512GL112)
	#include "efm32gg12b330f512gl112.h"

	#elif defined(EFM32GG12B330F512GQ100)
	#include "efm32gg12b330f512gq100.h"

	#elif defined(EFM32GG12B390F1024GL112)
	#include "efm32gg12b390f1024gl112.h"

	#elif defined(EFM32GG12B390F512GL112)
	#include "efm32gg12b390f512gl112.h"

	#elif defined(EFM32GG12B410F1024GL112)
	#include "efm32gg12b410f1024gl112.h"

	#elif defined(EFM32GG12B410F1024GL120)
	#include "efm32gg12b410f1024gl120.h"

	#elif defined(EFM32GG12B410F1024GM64)
	#include "efm32gg12b410f1024gm64.h"

	#elif defined(EFM32GG12B410F1024GQ100)
	#include "efm32gg12b410f1024gq100.h"

	#elif defined(EFM32GG12B410F1024GQ64)
	#include "efm32gg12b410f1024gq64.h"

	#elif defined(EFM32GG12B410F1024IL112)
	#include "efm32gg12b410f1024il112.h"

	#elif defined(EFM32GG12B410F1024IL120)
	#include "efm32gg12b410f1024il120.h"

	#elif defined(EFM32GG12B410F1024IM64)
	#include "efm32gg12b410f1024im64.h"

	#elif defined(EFM32GG12B410F1024IQ100)
	#include "efm32gg12b410f1024iq100.h"

	#elif defined(EFM32GG12B410F1024IQ64)
	#include "efm32gg12b410f1024iq64.h"

	#elif defined(EFM32GG12B430F512GL112)
	#include "efm32gg12b430f512gl112.h"

	#elif defined(EFM32GG12B430F512GL120)
	#include "efm32gg12b430f512gl120.h"

	#elif defined(EFM32GG12B430F512GM64)
	#include "efm32gg12b430f512gm64.h"

	#elif defined(EFM32GG12B430F512GQ100)
	#include "efm32gg12b430f512gq100.h"

	#elif defined(EFM32GG12B430F512GQ64)
	#include "efm32gg12b430f512gq64.h"

	#elif defined(EFM32GG12B430F512IL112)
	#include "efm32gg12b430f512il112.h"

	#elif defined(EFM32GG12B430F512IL120)
	#include "efm32gg12b430f512il120.h"

	#elif defined(EFM32GG12B430F512IM64)
	#include "efm32gg12b430f512im64.h"

	#elif defined(EFM32GG12B430F512IQ100)
	#include "efm32gg12b430f512iq100.h"

	#elif defined(EFM32GG12B430F512IQ64)
	#include "efm32gg12b430f512iq64.h"

	#elif defined(EFM32GG12B510F1024GL112)
	#include "efm32gg12b510f1024gl112.h"

	#elif defined(EFM32GG12B510F1024GL120)
	#include "efm32gg12b510f1024gl120.h"

	#elif defined(EFM32GG12B510F1024GM64)
	#include "efm32gg12b510f1024gm64.h"

	#elif defined(EFM32GG12B510F1024GQ100)
	#include "efm32gg12b510f1024gq100.h"

	#elif defined(EFM32GG12B510F1024GQ64)
	#include "efm32gg12b510f1024gq64.h"

	#elif defined(EFM32GG12B510F1024IL112)
	#include "efm32gg12b510f1024il112.h"

	#elif defined(EFM32GG12B510F1024IL120)
	#include "efm32gg12b510f1024il120.h"

	#elif defined(EFM32GG12B510F1024IM64)
	#include "efm32gg12b510f1024im64.h"

	#elif defined(EFM32GG12B510F1024IQ100)
	#include "efm32gg12b510f1024iq100.h"

	#elif defined(EFM32GG12B510F1024IQ64)
	#include "efm32gg12b510f1024iq64.h"

	#elif defined(EFM32GG12B530F512GL112)
	#include "efm32gg12b530f512gl112.h"

	#elif defined(EFM32GG12B530F512GL120)
	#include "efm32gg12b530f512gl120.h"

	#elif defined(EFM32GG12B530F512GM64)
	#include "efm32gg12b530f512gm64.h"

	#elif defined(EFM32GG12B530F512GQ100)
	#include "efm32gg12b530f512gq100.h"

	#elif defined(EFM32GG12B530F512GQ64)
	#include "efm32gg12b530f512gq64.h"

	#elif defined(EFM32GG12B530F512IL112)
	#include "efm32gg12b530f512il112.h"

	#elif defined(EFM32GG12B530F512IL120)
	#include "efm32gg12b530f512il120.h"

	#elif defined(EFM32GG12B530F512IM64)
	#include "efm32gg12b530f512im64.h"

	#elif defined(EFM32GG12B530F512IQ100)
	#include "efm32gg12b530f512iq100.h"

	#elif defined(EFM32GG12B530F512IQ64)
	#include "efm32gg12b530f512iq64.h"

	#elif defined(EFM32GG12B810F1024GL112)
	#include "efm32gg12b810f1024gl112.h"

	#elif defined(EFM32GG12B810F1024GL120)
	#include "efm32gg12b810f1024gl120.h"

	#elif defined(EFM32GG12B810F1024GM64)
	#include "efm32gg12b810f1024gm64.h"

	#elif defined(EFM32GG12B810F1024GQ100)
	#include "efm32gg12b810f1024gq100.h"

	#elif defined(EFM32GG12B810F1024GQ64)
	#include "efm32gg12b810f1024gq64.h"

	#elif defined(EFM32GG12B810F1024IL112)
	#include "efm32gg12b810f1024il112.h"

	#elif defined(EFM32GG12B810F1024IL120)
	#include "efm32gg12b810f1024il120.h"

	#elif defined(EFM32GG12B810F1024IM64)
	#include "efm32gg12b810f1024im64.h"

	#elif defined(EFM32GG12B810F1024IQ100)
	#include "efm32gg12b810f1024iq100.h"

	#elif defined(EFM32GG12B810F1024IQ64)
	#include "efm32gg12b810f1024iq64.h"

	#elif defined(EFM32GG12B830F512GL112)
	#include "efm32gg12b830f512gl112.h"

	#elif defined(EFM32GG12B830F512GL120)
	#include "efm32gg12b830f512gl120.h"

	#elif defined(EFM32GG12B830F512GM64)
	#include "efm32gg12b830f512gm64.h"

	#elif defined(EFM32GG12B830F512GQ100)
	#include "efm32gg12b830f512gq100.h"

	#elif defined(EFM32GG12B830F512GQ64)
	#include "efm32gg12b830f512gq64.h"

	#elif defined(EFM32GG12B830F512IL112)
	#include "efm32gg12b830f512il112.h"

	#elif defined(EFM32GG12B830F512IL120)
	#include "efm32gg12b830f512il120.h"

	#elif defined(EFM32GG12B830F512IM64)
	#include "efm32gg12b830f512im64.h"

	#elif defined(EFM32GG12B830F512IQ100)
	#include "efm32gg12b830f512iq100.h"

	#elif defined(EFM32GG12B830F512IQ64)
	#include "efm32gg12b830f512iq64.h"
	
	#else
	#error "em_device.h: PART NUMBER undefined"
	
	#endif

#endif	

#endif /* EM_DEVICE_H */
